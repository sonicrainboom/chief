package Registration

import (
	"crypto/ecdsa"
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"gitlab.com/sonicrainboom/chief/shared"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
	"net/http"
	"os"
	"regexp"
	"time"
)

var allowNonOfficialURLs = false

func GetPublicKeys(baseURL string) *jose.JSONWebKeySet {
	var jwks jose.JSONWebKeySet
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/api/v1/keys/instance", baseURL))
	if err != nil {
		return nil
	}
	if resp.StatusCode != 200 {
		return nil
	}

	_ = json.NewDecoder(resp.Body).Decode(&jwks)
	return &jwks
}

type BaseURLOnly struct {
	BaseURL string `json:"iss"`
}

func GetBaseURL(token string) (baseURL string, err error) {
	if token == "" {
		return "", errors.Wrap(util.ErrNoToken)
	}

	tok, err := jwt.ParseSigned(token)
	if err != nil {
		return "", errors.Wrap(err)
	}

	var bu BaseURLOnly
	err = tok.UnsafeClaimsWithoutVerification(&bu)
	if err != nil {
		return "", errors.Wrap(err)
	}

	if allowNonOfficialURLs || regexp.MustCompile(`(?m)^https://[^./\s]+.sonicrainboom.rocks$`).MatchString(bu.BaseURL) {
		return bu.BaseURL, nil
	} else {
		return "", jwt.ErrInvalidIssuer
	}
}

func ParseJWT(jwks *jose.JSONWebKeySet, token string, target interface{}) (err error) {
	if token == "" {
		return errors.Wrap(util.ErrNoToken)
	}

	tok, err := jwt.ParseSigned(token)
	if err != nil {
		return errors.Wrap(err)
	}

	var ei shared.ExpiryInfo
	err = tok.Claims(jwks, target, &ei)
	if err != nil {
		return errors.Wrap(err)
	}

	// Issued later than now (+ time drift)
	if time.Now().Add(30*time.Second).Unix() < ei.Issued {
		return errors.Wrap(jwt.ErrNotValidYet)
	}

	// Expired
	if ei.Expires != 0 && time.Now().Add(-30*time.Second).Unix() > ei.Expires {
		return errors.Wrap(jwt.ErrExpired)
	}

	return nil
}

func RegisterInstance(token string) (privateKey *ecdsa.PrivateKey, instanceUUID string, err error) {
	var baseURL string
	baseURL, err = GetBaseURL(token)
	if err != nil {
		err = errors.Wrap(err)
		return
	}
	var jwks = GetPublicKeys(baseURL)
	if jwks == nil {
		err = errors.Wrap(os.ErrNotExist)
		return
	}

	var irv shared.InstanceRegistrationVoucher
	err = ParseJWT(jwks, token, &irv)
	if err != nil {
		err = errors.Wrap(err)
		return
	}
	// We now know this token is authentic and we have our instanceUUID
	instanceUUID = irv.Subject

	return
}

func Allow() {
	allowNonOfficialURLs = true
}
