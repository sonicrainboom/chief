1. We do not use anything but technically necessary cookies. Those are
   - `chief_state`: A cookie temporarily set while logging in via one of the supported providers. It is used to prevent accidental re-submission of a login.
   - `chief_auth`: This cookie stores a JWT containing your account's UUID and an increasing counter. It signalizes which account you are logged into (if any). It is signed using a private key only known to us.
   - `chief_privacy`: This cookie is set once you consent this document. Either explicitly, or by logging into your account or performing any other action relating to account management on this site.
2. Additional cookies might be set by the provider you choose to log-in with. By clicking the respective button or link, you consent that the selected provider may store and/or use cookies and other data on your browser.
   - Our implementation of login providers does not require the use of their SDKs or tracking technology embedded on our site. Until you click the link or button of that provider, they have no knowledge of your visit to our site.
3. We do not store information about you. We only store an account identifier of a given provider in our database. We will not store it directly though, but store it in a pseudonomized way.
   - This pseudonomization will create a SHA256 hash, keyed with the Client ID using HMAC. `HMAC(SHA256, <ClientID>)(<provided account identifier>)`.
   - Without the raw account identifier (as we would temporarily see it during a log-in) we are unable to make any correlation with the stored identifier. Only the combination of the raw account identifier and our unique Client ID allows us to re-create this pseudonomized ID, making it possible to log you into your account. Without this information this pseudonomized ID is useless and cannot be tied to anyone.
4. You can delete your account at any time. For security reasons, you can only do so when logged into that account. Since we store no personally identifying information, it is impossible for us to verify an account outside of the regular login procedure.
   - Account deletion will not delete the actual account, as we need the record to maintain stable identifiers, but all information is made unusable with this procedure:
      1. All linked provider account identifiers (or derivates thereof, see 3.) associated to the account will be deleted from the database.
      2. A new account UUID is generated and prefixed with `SHREDDED_`. This will make the account inaccessible via the API.
      3. If there was an account display name set, it will be set the the same UUID.
      4. Counters for issued and revoked tokens (logins) are set to a static value, that is the same for all deleted accounts.
      5. The account's timestamps (creation, modification, deletion) are set to January 1st 1970.
   - Any server you might still have ownership of, will remain linked to the internal reference of the account's internal ID and show up as `orphaned`.
      - This internal ID is a monotonically increasing counter of accounts. There is no personally identifying information there.
      - If the display name of the linked server contains personally identifying information, you hereby consent, that this link will NOT be removed until the server is unlinked. This can be done on this site prior to account deletion, or in the server's administrative settings at any point.
      - This is done to protect the server from getting claimed by someone else. If you have access to the server's configuration, you can always link it to another account if you wish.
5. If you choose to not want to delete your account, but rather unlink a provider from it, you can do so by logging in, and clicking the `unlink` button of the provider you wish to unlink
   - Note: You can unlink all providers from an account. This will make it *impossible* to log-in, unless you link a provider. Act with caution.
      - While still logged in, you can link providers to your account. Once your session expires this will no longer be possible.
