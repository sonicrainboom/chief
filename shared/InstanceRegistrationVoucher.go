package shared


type InstanceRegistrationVoucher struct {
	Subject   string `json:"sub"`
	Issuer    string `json:"iss"`
	IssuedAt  int64  `json:"iat"`
	TokenType string `json:"token_type"`
}


type ExpiryInfo struct {
	Issued  int64 `json:"iat,omitempty"`
	Expires int64 `json:"exp,omitempty"`
}
