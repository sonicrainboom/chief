package util

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/hkdf"
	"io"
)

func Encrypt(passphrase string, plaintext string) (ciphertext, salt string) {
	var intermediate []byte
	var keys = make([][]byte, 2, 2)

	intermediateSalt := make([]byte, sha256.Size)
	if _, err := rand.Read(intermediateSalt); err != nil {
		panic(err)
	}

	df := hkdf.New(sha256.New, []byte(passphrase), intermediateSalt, nil)

	for i := 0; i < 2; i++ {
		keys[i] = make([]byte, aes.BlockSize)
		if _, err := io.ReadFull(df, keys[i]); err != nil {
			panic(err)
		}
	}

	block, err := aes.NewCipher(keys[0])
	if err != nil {
		panic(err.Error())
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	intermediate = gcm.Seal(nil, keys[1][:gcm.NonceSize()], []byte(plaintext), nil)

	ciphertext = fmt.Sprintf("%x", intermediate)
	salt = fmt.Sprintf("%x", intermediateSalt)
	return
}

func Decrypt(passphrase, salt, ciphertext string) (plaintext string) {
	var intermediateCiphertext, intermediateSalt, intermediatePlaintext []byte
	var keys = make([][]byte, 2, 2)
	var err error

	intermediateCiphertext, err = hex.DecodeString(ciphertext)
	if err != nil {
		panic(err.Error())
	}

	intermediateSalt, err = hex.DecodeString(salt)
	if err != nil {
		panic(err.Error())
	}

	df := hkdf.New(sha256.New, []byte(passphrase), intermediateSalt, nil)

	for i := 0; i < 2; i++ {
		keys[i] = make([]byte, aes.BlockSize)
		if _, err := io.ReadFull(df, keys[i]); err != nil {
			panic(err)
		}
	}

	block, err := aes.NewCipher(keys[0])
	if err != nil {
		panic(err.Error())
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	intermediatePlaintext, err = gcm.Open(nil, keys[1][:gcm.NonceSize()], intermediateCiphertext, nil)
	if err != nil {
		panic(err.Error())
	}

	plaintext = string(intermediatePlaintext)
	return
}
