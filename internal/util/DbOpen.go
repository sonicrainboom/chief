package util

import (
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"strings"
)

var DB *gorm.DB

func DBOpen() {
	var err error
	settings := []string{
		"_synchronous=3", // 2 is default
		"_case_sensitive_like=false",
		"cache=shared",
		"mode=rwc",
		"_journal_mode=WAL",
		"_loc=UTC",
	}
	DB, err = gorm.Open(sqlite.Open(config.Config.DatabasePath+"?"+strings.Join(settings, "&")), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	err = DB.Exec("PRAGMA wal_checkpoint(TRUNCATE);").Error
	if err != nil {
		panic(err)
	}
}
