package util

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/shared"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
	"gorm.io/gorm"
	"log"
	"math/big"
	"strings"
	"time"
)

var ChiefPassphrase = config.Config.KeyJarPassphrase

var ErrNoToken = errors.New("no token")

type KeyJarPrivateKey struct {
	gorm.Model
	KeyJarID     uint
	EncryptedKey string
	Salt         string
	KeyID        string
	privateKey   *ecdsa.PrivateKey `gorm:"-"`
	webkey       *jose.JSONWebKey  `gorm:"-"`
}

func (pk *KeyJarPrivateKey) DecryptIfRequired(passphrase string) {
	if pk.privateKey == nil || pk.webkey.Key == nil {
		pk.privateKey = stringToPrivateKey(Decrypt(passphrase, pk.Salt, pk.EncryptedKey))
	}
	key := privateKeyToWebKey(pk.privateKey)
	pk.webkey = &key
	if pk.KeyID != "" {
		pk.webkey.KeyID = pk.KeyID
	} else {
		pk.KeyID = pk.webkey.KeyID
	}
	return
}

func privateKeyToWebKey(ecdsaKey *ecdsa.PrivateKey) (key jose.JSONWebKey) {
	var err error
	if ecdsaKey == nil || ecdsaKey.D == nil {
		return
	}
	key = jose.JSONWebKey{
		Key:       &ecdsaKey.PublicKey,
		KeyID:     "_",
		Algorithm: string(jose.ES256),
		Use:       "sig",
	}
	var thumbprint []byte
	thumbprint, err = key.Thumbprint(crypto.SHA256)
	if err != nil {
		log.Fatal(err)
	}
	key.KeyID = fmt.Sprintf("%x", thumbprint[:8])
	return
}

func KeyJarPrivateKeyFromECDSA(key *ecdsa.PrivateKey, passphrase string) (jar KeyJarPrivateKey) {
	webkey := privateKeyToWebKey(key)
	jar.webkey = &webkey
	jar.KeyID = jar.webkey.KeyID
	jar.privateKey = key
	jar.EncryptedKey, jar.Salt = Encrypt(passphrase, privateKeyToString(key))
	return
}

/*
	KeyJar: Used to store JWT keys and restore them. Has a Signer property of type jose.Signer.
	The Private key stored in the DB is encrypted with a secret only known to Chief
*/
type KeyJar struct {
	gorm.Model
	JarName      string
	Signer       jose.Signer `gorm:"-"`
	SigningKeyID string
	JWKS         jose.JSONWebKeySet `gorm:"-"`
	// We cant store the privateKey in GORM, so we need to split it and join it
	PrivateKey []*KeyJarPrivateKey
}

func stringToPrivateKey(in string) (privateKey *ecdsa.PrivateKey) {
	splits := strings.Split(in, "|")
	if len(splits) != 3 {
		return nil
	}

	privateKey = &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: elliptic.P256(),
			X:     &big.Int{},
			Y:     &big.Int{},
		},
		D: &big.Int{},
	}

	var ok bool
	if _, ok = privateKey.D.SetString(splits[0], 10); !ok {
		log.Fatal("Invalid D")
		return nil
	}
	if _, ok = privateKey.X.SetString(splits[1], 10); !ok {
		log.Fatal("Invalid X")
		return nil
	}
	if _, ok = privateKey.Y.SetString(splits[2], 10); !ok {
		log.Fatal("Invalid Y")
		return nil
	}
	return
}

func privateKeyToString(privateKey *ecdsa.PrivateKey) (out string) {
	if privateKey == nil {
		return ""
	}
	return fmt.Sprintf("%s|%s|%s", privateKey.D.String(), privateKey.X.String(), privateKey.Y.String())
}

func (jar *KeyJar) AddPrivateKey(privateKey *KeyJarPrivateKey, forceAsSignKey bool) {
	var err error
	var saveKeyJar bool = true
	var signingKey *KeyJarPrivateKey
	privateKey.DecryptIfRequired(ChiefPassphrase)

	for _, key := range jar.PrivateKey {
		if privateKey.KeyID == key.KeyID {
			// We have the privateKey, no need to save.
			saveKeyJar = false
		}
	}
	if saveKeyJar {
		jar.PrivateKey = append(jar.PrivateKey, privateKey)
	}

	jar.JWKS = jose.JSONWebKeySet{Keys: nil}
	for _, key := range jar.PrivateKey {
		key.DecryptIfRequired(ChiefPassphrase)
		if signingKey == nil || key.CreatedAt.After(signingKey.CreatedAt) {
			signingKey = key
		}
		jar.JWKS.Keys = append(jar.JWKS.Keys, *key.webkey)
		log.Printf("Loaded key %s/%s\n", jar.JarName, key.KeyID)
	}
	if forceAsSignKey {
		signingKey = privateKey
	}
	if signingKey == nil {
		log.Fatal("no signing key")
	}

	jar.SigningKeyID = signingKey.KeyID
	log.Printf("Signing key for %s: %s\n", jar.JarName, jar.SigningKeyID)

	if saveKeyJar {
		err := DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(&jar).Error
		if err != nil {
			log.Fatal(err)
		}
	}

	jar.Signer, err = jose.NewSigner(
		jose.SigningKey{Algorithm: jose.ES256, Key: signingKey.privateKey},
		(&jose.SignerOptions{ExtraHeaders: map[jose.HeaderKey]interface{}{"kid": jar.SigningKeyID}}).WithType("JWT"),
	)
	if err != nil {
		panic(err)
	}
}

func GetKeyJar(name string, autoGenerateKeyIfMissing bool) *KeyJar {
	var err error
	var jar KeyJar
	jar.JarName = name

	err = DB.FirstOrCreate(&jar, &jar).Error
	if err != nil {
		log.Fatal(err)
	}

	err = DB.Find(&jar.PrivateKey, &KeyJarPrivateKey{
		KeyJarID: jar.ID,
	}).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		log.Fatal(err)
	}

	if autoGenerateKeyIfMissing {
		jar.GenerateKeyIfNone()
	}

	for _, key := range jar.PrivateKey {
		jar.AddPrivateKey(key, false)
	}

	return &jar
}

func (jar *KeyJar) GenerateKeyIfNone() {
	var err error
	var key *ecdsa.PrivateKey
	if len(jar.PrivateKey) == 0 {
		key, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if err != nil {
			log.Fatal(err)
		}

		pk := KeyJarPrivateKeyFromECDSA(key, ChiefPassphrase)

		jar.AddPrivateKey(&pk, false)
	}
}

func (jar *KeyJar) CreateJWT(payload interface{}) (token string, err error) {
	var buf []byte

	buf, err = json.Marshal(payload)
	if err != nil {
		return "", errors.Wrap(err)
	}
	raw, err := jar.Signer.Sign(buf)
	if err != nil {
		return "", errors.Wrap(err)
	}
	token, err = raw.CompactSerialize()
	if err != nil {
		return "", errors.Wrap(err)
	}
	return
}

func (jar *KeyJar) ParseJWT(token string, target interface{}) (err error) {
	if token == "" {
		return ErrNoToken
	}

	tok, err := jwt.ParseSigned(token)
	if err != nil {
		return errors.Wrap(err)
	}

	var ei shared.ExpiryInfo
	err = tok.Claims(&jar.JWKS, target, &ei)
	if err != nil {
		return errors.Wrap(err)
	}

	// Issued later than now (+ time drift)
	if time.Now().Add(30*time.Second).Unix() < ei.Issued {
		return errors.Wrap(jwt.ErrNotValidYet)
	}

	// Expired
	if ei.Expires != 0 && time.Now().Add(-30*time.Second).Unix() > ei.Expires {
		return errors.Wrap(jwt.ErrExpired)
	}

	return nil
}
