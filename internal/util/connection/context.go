package connection

import (
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"log"
	"net"
	"net/http"
	"strings"
)

var trustedProxies []*net.IPNet

func init() {
	var err error
	var n *net.IPNet
	for _, proxy := range config.Config.TrustedProxies {
		proxy = strings.TrimSpace(proxy)
		if proxy == "" {
			continue
		}
		_, n, err = net.ParseCIDR(strings.TrimSpace(proxy))
		if err != nil {
			log.Print(err)
			continue
		}
		log.Printf("Adding %s as trusted proxy\n", n.String())
		trustedProxies = append(trustedProxies, n)
	}
}

var ConnContextKey = struct{}{}

func IsTrustedProxy(conn net.Conn) bool {
	var host string
	var ip net.IP
	var err error
	host, _, err = net.SplitHostPort(conn.RemoteAddr().String())
	if err != nil {
		return false
	}

	ip = net.ParseIP(host)

	for _, n := range trustedProxies {
		if n.Contains(ip) {
			return true
		}
	}
	return false
}

func GetRealIP(r *http.Request) (ip net.IP, err error) {
	conn, ok := r.Context().Value(ConnContextKey).(net.Conn)
	if !ok {
		return nil, errors.New("not a connection with context")
	}
	if xff := r.Header.Get("X-Forwarded-For"); xff != "" && IsTrustedProxy(conn) {
		// Get the most-right IP. That is the IP connected to the proxy.
		ips := strings.Split(xff, ",")
		last := ips[len(ips)-1]
		ip = net.ParseIP(strings.TrimSpace(last))
	}
	if ip == nil || ip.IsUnspecified() {
		var host string
		host, _, err = net.SplitHostPort(conn.RemoteAddr().String())
		if err != nil {
			return
		}
		ip = net.ParseIP(host)
	}
	return
}
