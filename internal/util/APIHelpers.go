package util

import (
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"io"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"strings"
)

var BaseURL = "https://account.sonicrainboom.rocks"

const MAX_PAYLOAD_BYTES = 512

type dumbBuffer struct {
	maxSize int
	buf     []byte
	wpos    int
	rpos    int
}

func newDumbBuffer(maxSize int) (buf *dumbBuffer) {
	buf = new(dumbBuffer)
	buf.maxSize = maxSize
	buf.buf = make([]byte, 128, maxSize)
	return
}

func (d *dumbBuffer) Write(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	n = copy(d.buf[d.wpos:cap(d.buf)], p)
	if n != len(p) {
		err = io.ErrShortWrite
	}
	d.wpos += n
	return
}

func (d *dumbBuffer) Len() int {
	return d.wpos
}

func (d *dumbBuffer) Read(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, io.ErrShortBuffer
	}
	if d.rpos >= d.wpos {
		return 0, io.EOF
	}

	n = copy(p, d.buf[d.rpos:d.wpos])
	if n < len(p) {
		err = io.EOF
	}
	d.rpos += n
	return
}

/*
	RenderJSON: Renders the passed val as JSON with pretty print.
	The passes status is set, unless rendering the JSON causes an error, then the code will be 500.
	If status == 0, no headers will be set. It is assumed it was done before.
	ATTENTION! If val is a slice or array of any type, and nil, it will be rendered as an empty slice, not `null`.
	This is NOT standard json.Encoder behaviour, but a custom implementation detail. Any nested structure is not touched.

	The rendered JSON is guaranteed to fit in MAX_PAYLOAD_BYTES bytes.
	The actualStatus that was sent is returned.
	A call of this function guarantees a response to be sent.
*/
func RenderJSON(writer http.ResponseWriter, val interface{}, status int) (actualStatus int) {
	var written int64 = 0
	var err error
	var encoder *json.Encoder
	var buf *dumbBuffer
	defer func() {
		if err, ok := recover().(error); ok && err != nil {
			if errors.Is(err, io.ErrShortWrite) {
				log.Printf("RenderJSON(): ATTENTION! Endpoint tried to write more than %0.f bytes!\n", MAX_PAYLOAD_BYTES)
			} else {
				log.Printf("RenderJSON(): %+v\n", err)
			}
			if written == 0 && status != 0 {
				// Make sure nothing of this gets cached
				writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
				writer.Header().Del("Content-Length")
				writer.WriteHeader(500)
			}
			actualStatus = 500
		}
	}()
	actualStatus = status

	/*
		Why do we use a buffer instead of writing to the stream directly?
		1. This allows us to check for errors (and thus, change the status code) before sending the body
			- We could do this with a regular Marshall(), too, but:
		2. We can easily control the max size and quit early if it becomes too big.
			- Should there be a memory leaking vulnerability somewhere in what we encode, we can at least
			  try to limit what could be leaked.
		3. We can determine the Content-Length
	*/
	buf = newDumbBuffer(MAX_PAYLOAD_BYTES)

	/*
		A nil slice would yield a `null` to be rendered.
		If we just allocate an empty slice (an empty JSON array will look the same regardless of type) we can trick it
		into rendering `[]` instead of `null`.
	*/
	if sliceCandidate := reflect.Indirect(reflect.ValueOf(val)); sliceCandidate.Kind() == reflect.Slice {
		if sliceCandidate.Len() == 0 {
			val = make([]interface{}, 0, 0)
		}
	}

	// Encode the JSON
	encoder = json.NewEncoder(buf)
	encoder.SetIndent("", "  ")
	err = encoder.Encode(val)
	if err != nil {
		err = errors.Wrap(err)
		panic(err) // This will we `recover()`ed above.
	}

	// All clear, send to client
	if status != 0 {
		writer.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
		writer.WriteHeader(actualStatus)
	}
	written, err = io.Copy(writer, buf)
	if err != nil {
		err = errors.Wrap(err)
		panic(err) // This will we `recover()`ed above.
	}
	return
}

func EndRequest(writer http.ResponseWriter, request *http.Request, statusCode int, queryParams ...string) (redirectedToUI bool) {
	if request.URL.Query().Get("ui") == "true" {
		writer.Header().Set("Location", fmt.Sprintf("/?status=%d&%s", statusCode, strings.Join(queryParams, "&")))
		writer.WriteHeader(302)
		redirectedToUI = true
	} else {
		writer.WriteHeader(statusCode)
	}
	return
}

func DidRevalidateExplicitCache(writer http.ResponseWriter, r *http.Request, ETag string) (cacheRefreshed bool) {
	writer.Header().Set("Cache-Control", "public, max-age=30, no-transform, stale-while-revalidate=240, stale-if-error=604800")
	if ETag != "" {
		writer.Header().Set("Etag", ETag)
		if r.Header.Get("If-None-Match") == ETag {
			writer.WriteHeader(304)
			log.Printf("Auth Keys cache refresh via ETag")
			return true
		}
	}
	return false
}

func WritePrivacyCookie(writer http.ResponseWriter) {
	privacyCookie := &http.Cookie{Name: "chief_privacy", Path: "/", Value: "1", HttpOnly: true, SameSite: http.SameSiteLaxMode}

	http.SetCookie(writer, privacyCookie)
}

func IsPrivacyCookieSet(r *http.Request) bool {
	cookie, err := r.Cookie("chief_privacy")
	if err != nil {
		return false
	}
	return cookie.Value == "1"
}
