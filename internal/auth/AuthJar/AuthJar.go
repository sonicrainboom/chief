package AuthJar

import (
	"encoding/json"
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"math"
	"net/http"
	"os"
	"sync"
	"time"
)

const AUTH_COOKIE_NAME = "chief_auth"
const AUTH_COOKIE_SCOPE = "/"

var KeyJar *util.KeyJar
var mtx sync.Mutex

/*
	AuthJar: Is used to act as a bearer as a JWT. Only the UUID must be set, the rest will be replaced with fresh values
	from the database upon JWT conversion.
*/
type AuthJar struct {
	UUID       string `json:"account_uuid"`
	TokenIndex uint   `json:"token_index"`
	Issued     int64  `json:"iat,omitempty"`
	Expires    int64  `json:"exp,omitempty"`
}

func Init() {
	mtx.Lock()
	defer mtx.Unlock()
	if KeyJar == nil {
		KeyJar = util.GetKeyJar("auth", true)
	}
}

func (j *AuthJar) WriteCookie(writer http.ResponseWriter) {
	var token string
	token = j.ToJWT()

	stateCookie := &http.Cookie{
		Name:     AUTH_COOKIE_NAME,
		Path:     AUTH_COOKIE_SCOPE,
		Expires:  time.Unix(j.Expires, 0),
		Value:    token,
		HttpOnly: true,
		SameSite: http.SameSiteLaxMode,
	}

	http.SetCookie(writer, stateCookie)
}

func (j *AuthJar) GetUser() (user *types.User, err error) {
	if j == nil || j.UUID == "" {
		return nil, os.ErrInvalid
	}
	user = &types.User{}
	user.PublicUUID = j.UUID
	err = util.DB.Find(user, user).Error
	//TODO Check if error is No such record
	if err != nil {
		return nil, err
	}
	if !user.IsValid() || j.TokenIndex <= user.LastRevokedIndex {
		return nil, os.ErrInvalid
	}
	return
}

func (j *AuthJar) ToJWT() (token string) {
	if j.UUID == "" {
		return ""
	}

	// We need to set this, so `GetUser()` does not fail on the revocation check
	j.TokenIndex = math.MaxUint32

	user, err := j.GetUser()
	if err != nil {
		return ""
	}
	if user.LastRevokedIndex > user.TokensIssued {
		user.TokensIssued = user.LastRevokedIndex
	}
	user.TokensIssued++

	j.TokenIndex = user.TokensIssued
	j.Issued = time.Now().Unix()
	j.Expires = time.Now().Add(24 * time.Hour).Unix()

	js, err := json.Marshal(j)
	if err != nil {
		return ""
	}

	raw, err := KeyJar.Signer.Sign(js)
	if err != nil {
		return ""
	}

	token, err = raw.CompactSerialize()
	if err != nil {
		return ""
	}

	err = util.DB.Save(user).Error
	if err != nil {
		return ""
	}
	return
}

func AuthJarByRequest(request *http.Request) (j *AuthJar, err error) {
	cookie, err := request.Cookie(AUTH_COOKIE_NAME)
	if err != nil {
		err = errors.Wrap(err)
		return
	}
	if cookie.Value == "" {
		err = errors.Wrap(http.ErrNoCookie)
		return
	}

	j, err = AuthJarByToken(cookie.Value)
	if err != nil {
		err = errors.Wrap(err)
	}
	return
}

func ResetAuthCookie(writer http.ResponseWriter) {
	stateCookie := &http.Cookie{Name: AUTH_COOKIE_NAME, Path: AUTH_COOKIE_SCOPE, Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}
	http.SetCookie(writer, stateCookie)
}

func AuthJarByToken(token string) (j *AuthJar, err error) {
	var jar AuthJar
	err = KeyJar.ParseJWT(token, &jar)
	if err != nil {
		err = errors.Wrap(err)
	}

	return &jar, nil
}
