package GitLab

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/auth/providers"
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
	"io/ioutil"
	"regexp"
)

var r = regexp.MustCompile(`^[0-9]+$`)

type GitLabUser struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func init() {
	var gl = providers.Provider(
		GitLab{
			Config: &oauth2.Config{
				ClientID:     config.Config.GitLab.ClientID,
				ClientSecret: config.Config.GitLab.ClientSecret,
				Scopes:       []string{"read_user"},
				Endpoint:     gitlab.Endpoint,
			},
		},
	)

	providers.AddProvider(providers.ProviderDescriptor{
		Name:               "GitLab.com",
		Enabled:            true,
		IconClass:          "fab fa-gitlab",
		ImplementationName: "gitlab.com",
	}, gl)
}

type GitLab struct {
	Config *oauth2.Config
}

func (g GitLab) Oauth2Config(tenant string) *oauth2.Config {
	if tenant != "" {
		return nil
	}

	return g.Config
}

func (g GitLab) ValidateTenant(tenant string) bool {
	return tenant == ""
}

func (g GitLab) ValidateProviderUserID(userID string, tenant string) bool {
	return r.MatchString(userID) && tenant == ""
}

func (g GitLab) GetUserID(tenant string, token *oauth2.Token) (userID string, err error) {
	var gitlabUser GitLabUser
	var ctx = context.Background()

	client := g.Oauth2Config(tenant).Client(ctx, token)
	resp, err := client.Get("https://gitlab.com/api/v4/user")
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Response != 200: %d", resp.StatusCode))
	}

	data, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(data, &gitlabUser)
	if err != nil {
		return
	}

	if gitlabUser.ID == 0 {
		return "", errors.New("gitlabUser.ID == 0")
	}
	userID = fmt.Sprintf("%d", gitlabUser.ID)
	return
}
