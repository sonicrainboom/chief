package AppleID

import (
	"context"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/auth/providers"
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"golang.org/x/oauth2"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	// Import for early DB access
	_ "gitlab.com/sonicrainboom/chief/internal/database"
)

// https://developer.apple.com/documentation/sign_in_with_apple/tokenresponse
type AppleIDTokenResponse struct {
	ExpiresIn    int64  `json:"expires_in"`
	AccessToken  string `json:"access_token"`
	IDToken      string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
}

// https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens # Creating the Client Secret
type AppleIDClientSecret struct {
	KeyID     string `json:"kid"`
	Issuer    string `json:"iss"`
	IssuedAt  int64  `json:"iat"`
	ExpiresAt int64  `json:"exp"`
	Audience  string `json:"aud"`
	Subject   string `json:"sub"`
}

func init() {
	var pk *ecdsa.PrivateKey
	var blocks []byte
	var err error
	var keyJar *util.KeyJar
	keyJar = util.GetKeyJar("provider_apple_id", false)

	if keyJar.SigningKeyID != config.Config.AppleID.KeyIdentifier {
		blocks, err = ioutil.ReadFile(config.Config.AppleID.PrivateKeyPath)
		if err != nil {
			panic(err)
		}
		var in interface{}
		var ok bool

		block, _ := pem.Decode(blocks)
		if block == nil {
			log.Fatal(block)
		}

		in, err = x509.ParsePKCS8PrivateKey(block.Bytes)
		if pk, ok = in.(*ecdsa.PrivateKey); !ok || err != nil {
			panic("not *ecdsa.PrivateKey")
		}

		key := util.KeyJarPrivateKeyFromECDSA(pk, util.ChiefPassphrase)
		key.KeyID = config.Config.AppleID.KeyIdentifier

		keyJar.AddPrivateKey(&key, true)
	}

	var appleID = providers.Provider(
		AppleID{
			Config: &oauth2.Config{
				Endpoint: oauth2.Endpoint{
					AuthURL:   "https://appleid.apple.com/auth/authorize",
					TokenURL:  "https://appleid.apple.com/auth/token",
					AuthStyle: oauth2.AuthStyleInParams,
				},
			},
			Signer: keyJar.Signer,
		},
	)

	providers.AddProvider(providers.ProviderDescriptor{
		Name:               "Sign in with Apple",
		Enabled:            true,
		IconClass:          "fab fa-apple",
		ImplementationName: "apple-id",
	}, appleID)
}

type AppleID struct {
	Config *oauth2.Config
	Signer jose.Signer
}

func (a AppleID) Oauth2Config(tenant string) *oauth2.Config {
	if tenant != "" {
		return nil
	}

	var cfg oauth2.Config
	cfg.Endpoint = a.Config.Endpoint
	cfg.ClientID = config.Config.AppleID.ServiceID
	cfg.RedirectURL = a.Config.RedirectURL
	cfg.Scopes = []string{}
	cfg.ClientSecret = a.GenerateSecret()

	return &cfg
}

func (a AppleID) ValidateTenant(tenant string) bool {
	return tenant == ""
}

func (a AppleID) ValidateProviderUserID(userID string, tenant string) bool {
	return userID != "" && tenant == ""
}

func (a *AppleID) GenerateSecret() (token string) {
	var secret AppleIDClientSecret

	secret.KeyID = config.Config.AppleID.KeyIdentifier
	secret.Issuer = config.Config.AppleID.DeveloperTeamID
	secret.IssuedAt = time.Now().Add(-90 * time.Second).Unix()
	secret.ExpiresAt = time.Now().Add(10 * time.Minute).Unix()
	secret.Audience = "https://appleid.apple.com"
	secret.Subject = config.Config.AppleID.ServiceID

	js, err := json.Marshal(secret)
	if err != nil {
		return ""
	}
	raw, err := a.Signer.Sign(js)
	if err != nil {
		return ""
	}
	token, _ = raw.CompactSerialize()
	return
}

func (a AppleID) GetPublicKeys() *jose.JSONWebKeySet {
	// TODO Cache this
	var jwks jose.JSONWebKeySet
	resp, err := http.DefaultClient.Get("https://appleid.apple.com/auth/keys")
	if err != nil {
		return nil
	}
	if resp.StatusCode != 200 {
		return nil
	}

	_ = json.NewDecoder(resp.Body).Decode(&jwks)
	return &jwks
}

func (a AppleID) GetUserID(tenant string, token *oauth2.Token) (userID string, err error) {
	var response AppleIDTokenResponse
	var ctx = context.Background()

	client := a.Oauth2Config(tenant).Client(ctx, token)

	// https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
	resp, err := client.PostForm(
		"https://appleid.apple.com/auth/token",
		url.Values{
			"client_id":     []string{config.Config.AppleID.ServiceID},
			"client_secret": []string{a.GenerateSecret()},
			"grant_type":    []string{"refresh_token"},
			"refresh_token": []string{token.RefreshToken},
		},
	)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Response != 200: %d", resp.StatusCode))
	}

	data, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(data, &response)
	if err != nil {
		return
	}

	if response.IDToken == "" {
		return "", errors.New("no id_token")
	}

	// Get actual ID from token
	tok, err := jwt.ParseSigned(response.IDToken)
	if err != nil {
		log.Print(err)
		return "", errors.New("invalid id_token")
	}

	var jar struct {
		Issued   int64  `json:"iat"`
		Expires  int64  `json:"exp"`
		Audience string `json:"aud"`
		Subject  string `json:"sub"`
		AtHash   string `json:"at_hash"`
	}
	err = tok.Claims(a.GetPublicKeys(), &jar)
	if err != nil {
		log.Print(err)
		return "", errors.New("invalid id_token (signature)")
	}

	// Issued later than now (+ time drift)
	if time.Now().Add(30*time.Second).Unix() < jar.Issued {
		return "", errors.New("invalid id_token (iat)")
	}

	// Expired
	if jar.Expires != 0 && time.Now().Add(-30*time.Second).Unix() > jar.Expires {
		return "", errors.New("invalid id_token (exp)")
	}

	if jar.Audience != config.Config.AppleID.ServiceID {
		return "", errors.New("invalid id_token (aud)")
	}
	if jar.Subject == "" {
		return "", errors.New("invalid id_token (aud)")
	}

	return jar.Subject, nil
}
