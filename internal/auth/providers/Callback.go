package providers

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sonicrainboom/chief/internal/auth/AuthJar"
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"os"
)

func Callback(writer http.ResponseWriter, request *http.Request) {
	var err error
	// The user the provider account was assigned to (might be a new account)
	var newUser *types.User
	var auth *AuthJar.AuthJar
	var vars = mux.Vars(request)
	var providerName = vars["provider"]
	var tenant = vars["tenant"]
	var resetAuth bool
	var returnToUI bool
	var statusCode int = 500

	util.WritePrivacyCookie(writer)

	auth, newUser, resetAuth, returnToUI, err = handleCallback(providerName, tenant, request)
	ResetStateCookie(writer) // Regardless of status, clear state!
	// 400/500
	if err != nil {
		err = errors.Wrapf(err, "handleCallback failed")
		log.Printf("%+v", err)
		if errors.Is(err, os.ErrInvalid) {
			statusCode = 400
		} else {
			statusCode = 500
		}
	}
	if auth == nil || newUser == nil {
		statusCode = 400
	} else {
		statusCode = 200
	}

	if statusCode >= 400 {
		if resetAuth {
			AuthJar.ResetAuthCookie(writer)
		}
	} else {
		auth.WriteCookie(writer)
	}

	if returnToUI {
		// This is queried in EndRequest()
		request.URL.RawQuery += "&ui=true"
	}

	if !util.EndRequest(writer, request, statusCode, fmt.Sprintf("postLoginStatus=%d", statusCode)) {
		util.RenderJSON(writer, auth, 0)
	}
}

/*
	handleCallback: Returns err == os.Invalid on 400, err != nil on 500.
	If err == nil, auth is non-nil and user is non-nil, we are successful
	If resetAuth is true, the caller MUST clear the auth jar cookie.
*/
func handleCallback(providerName string, tenant string, request *http.Request) (
	auth *AuthJar.AuthJar,
	newUser *types.User,
	resetAuth bool,
	returnToUI bool,
	err error,
) {
	// newUser: The user the provider account was assigned to (might be a new account)
	// loggedInUser: The user detected from existing authentication
	var loggedInUser *types.User
	var provider Provider
	var jar *StateJar

	jar, err = StateJarByRequest(request)
	if jar == nil {
		err = errors.Wrapf(err, "could not get StateJarByRequest")
		log.Print(err)
		return
	}
	returnToUI = jar.ReturnToUI

	provider, err = GetProvider(providerName)
	if err != nil {
		err = errors.Wrapf(err, "could not GetProvider")
		log.Print(err)
		return
	}

	if !provider.ValidateTenant(tenant) {
		err = errors.Wrapf(os.ErrInvalid, "!provider.ValidateTenant")
		return
	}

	if !jar.Validate(providerName, tenant, request) {
		err = errors.Wrapf(os.ErrInvalid, "!jar.Validate")
		return
	}

	if string(jar.StateToken) != request.URL.Query().Get("state") {
		err = errors.Wrapf(os.ErrInvalid, "StateToken does not match query")
		return
	}

	// Cool we got our state jar back, and everything is authentic.
	code := request.URL.Query().Get("code")
	if code == "" {
		err = errors.Wrapf(os.ErrInvalid, "code is empty")
		return
	}

	ctx := context.Background()
	config := provider.Oauth2Config(tenant)

	token, err := config.Exchange(ctx, code)
	if err != nil {
		err = errors.Wrapf(err, "Exchange failed")
		return
	}

	var providerUserID string
	providerUserID, err = provider.GetUserID(tenant, token)
	if err != nil {
		err = errors.Wrapf(err, "provider.GetUserID failed")
		return
	}

	providerUserID = hashProviderUserID(config, providerUserID)

	auth, err = AuthJar.AuthJarByRequest(request)
	if auth != nil {
		loggedInUser, err = auth.GetUser()
		err = errors.Wrapf(err, "GetUser failed")
		// Reset auth if the user was invalid or the token revoked
		resetAuth = errors.Is(err, os.ErrInvalid)
	} else {
		err = errors.Wrapf(err, "AuthJarByRequest failed")
		auth = &AuthJar.AuthJar{}
		// Don't reset auth if we just did not have a cookie
		resetAuth = errors.Is(err, http.ErrNoCookie)
	}

	newUser, err = NewProviderUserID(providerName, tenant, providerUserID, loggedInUser)
	if err != nil {
		err = errors.Wrapf(err, "NewProviderUserID failed")
		auth = nil
		newUser = nil
		resetAuth = true
		return
	}
	auth.UUID = newUser.PublicUUID
	return
}

func hashProviderUserID(config *oauth2.Config, ID string) string {
	mac := hmac.New(sha256.New, []byte(config.ClientID))
	mac.Write([]byte(ID))
	return fmt.Sprintf("%x", mac.Sum(nil))
}
