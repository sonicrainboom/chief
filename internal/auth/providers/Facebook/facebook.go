package GitLab

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/auth/providers"
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/facebook"
	"io/ioutil"
	"regexp"
)

var r = regexp.MustCompile(`^[0-9]+$`)

type FacebookUser struct {
	ID string `json:"id"`
}

func init() {
	var gl = providers.Provider(
		Facebook{
			Config: &oauth2.Config{
				ClientID:     config.Config.Facebook.ClientID,
				ClientSecret: config.Config.Facebook.ClientSecret,
				Scopes:       []string{},
				Endpoint:     facebook.Endpoint,
			},
		},
	)

	providers.AddProvider(providers.ProviderDescriptor{
		Name:               "Facebook",
		Enabled:            true,
		IconClass:          "fab fa-facebook",
		ImplementationName: "facebook",
	}, gl)
}

type Facebook struct {
	Config *oauth2.Config
}

func (g Facebook) Oauth2Config(tenant string) *oauth2.Config {
	if tenant != "" {
		return nil
	}

	return g.Config
}

func (g Facebook) ValidateTenant(tenant string) bool {
	return tenant == ""
}

func (g Facebook) ValidateProviderUserID(userID string, tenant string) bool {
	return r.MatchString(userID) && tenant == ""
}

func (g Facebook) GetUserID(tenant string, token *oauth2.Token) (userID string, err error) {
	var facebookUser FacebookUser
	var ctx = context.Background()

	client := g.Oauth2Config(tenant).Client(ctx, token)
	resp, err := client.Get("https://graph.facebook.com/v8.0/me?fields=id")
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Response != 200: %d", resp.StatusCode))
	}

	data, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(data, &facebookUser)
	if err != nil {
		return
	}

	if facebookUser.ID == "" {
		return "", errors.New("facebookUser.ID empty")
	}
	userID = facebookUser.ID
	return
}
