package providers

import (
	"crypto/rand"
	"fmt"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"gitlab.com/sonicrainboom/chief/internal/util/connection"
	"golang.org/x/crypto/sha3"
	"gopkg.in/square/go-jose.v2"
	"gorm.io/gorm"
	"net"
	"net/http"
	"os"
	"regexp"
	"sync"
	"time"
)

type StateToken string

const STATE_COOKIE_NAME = "chief_state"
const STATE_COOKIE_SCOPE = "/api/v1/auth"

var conf *util.KeyJar
var mtx sync.Mutex

var r = regexp.MustCompile(`^[a-z0-9]{64}$`)

func (t StateToken) ValidFormat() bool {
	return r.MatchString(string(t))
}

type StateJar struct {
	Implementation string     `json:"-"`
	Tenant         string     `json:"-"`
	SourceIP       net.IP     `json:"-"`
	UserAgent      string     `json:"-"`
	Nonce          string     `json:"nonce"`
	StateToken     StateToken `json:"state"`
	ReturnToUI     bool       `json:"return_to_ui,omitempty"`
	Issued         int64      `json:"iat,omitempty"`
	Expires        int64      `json:"exp,omitempty"`
}

type StateJarConfig struct {
	gorm.Model
	Key        string
	rawKey     []byte
	signer     jose.Signer
	signingKey jose.SigningKey
}

func Init() {
	mtx.Lock()
	defer mtx.Unlock()
	if conf == nil {
		conf = util.GetKeyJar("state", true)
	}
}

func MakeStateJar(implementation string, tenant string, r *http.Request) (state *StateJar, err error) {
	var ip net.IP
	ip, err = connection.GetRealIP(r)
	if err != nil {
		return
	}

	var buf [32]byte
	_, err = rand.Read(buf[:])
	if err != nil {
		return
	}

	state = &StateJar{
		Implementation: implementation,
		Tenant:         tenant,
		SourceIP:       ip,
		UserAgent:      r.Header.Get("User-Agent"),
		Nonce:          fmt.Sprintf("%x", buf),
	}

	return
}

func (j *StateJar) Validate(implementation string, tenant string, r *http.Request) bool {
	var ip net.IP
	var err error
	var newToken StateToken
	ip, err = connection.GetRealIP(r)
	if err != nil {
		return false
	}

	if j == nil || j.Nonce == "" || j.StateToken == "" {
		return false
	}

	newToken = StateToken(fmt.Sprintf("%x", sha3.Sum256([]byte(fmt.Sprintf("%s|%s|%s|%s|%s", implementation, tenant, ip.String(), r.Header.Get("User-Agent"), j.Nonce)))))

	return newToken == j.StateToken
}

func (j *StateJar) BuildToken() error {
	if j.StateToken.ValidFormat() {
		goto wipe
	}

	if j.SourceIP == nil || j.SourceIP.IsUnspecified() || j.Nonce == "" {
		return os.ErrInvalid
	}

	j.StateToken = StateToken(fmt.Sprintf("%x", sha3.Sum256([]byte(fmt.Sprintf("%s|%s|%s|%s|%s", j.Implementation, j.Tenant, j.SourceIP.String(), j.UserAgent, j.Nonce)))))

wipe:
	j.SourceIP = nil
	j.UserAgent = ""
	return nil
}

func (j *StateJar) WriteCookie(writer http.ResponseWriter) {
	var token string
	token = j.ToJWT()

	stateCookie := &http.Cookie{Name: STATE_COOKIE_NAME, Path: STATE_COOKIE_SCOPE, Value: token, HttpOnly: true, SameSite: http.SameSiteLaxMode}

	http.SetCookie(writer, stateCookie)
}

func (j *StateJar) ToJWT() (token string) {
	err := j.BuildToken()
	if err != nil {
		return ""
	}

	j.Issued = time.Now().Unix()
	j.Expires = time.Now().Add(10 * time.Minute).Unix()

	token, _ = conf.CreateJWT(j)
	return
}

func StateJarByRequest(request *http.Request) (j *StateJar, err error) {
	var cookie *http.Cookie
	cookie, err = request.Cookie(STATE_COOKIE_NAME)
	if err != nil {
		err = errors.Wrap(err)
		return
	}

	return StateJarByToken(cookie.Value)
}

func ResetStateCookie(writer http.ResponseWriter) {
	stateCookie := &http.Cookie{Name: STATE_COOKIE_NAME, Path: STATE_COOKIE_SCOPE, Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}
	http.SetCookie(writer, stateCookie)
}

func StateJarByToken(token string) (j *StateJar, err error) {
	var jar StateJar
	err = conf.ParseJWT(token, &jar)
	if err != nil {
		err = errors.Wrap(err)
	}

	return &jar, nil
}
