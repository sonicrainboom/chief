package providers

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"gorm.io/gorm"
	"math"
	"os"
	"time"
)

/*
	NewProviderUserID: Takes the providerName, tenant and providerUserID to lookup or create the Provider record.
	If the record is not assigned to a user, it will either:
	1. Assign it to the registeredUser passed in
	2. Create a new Subject and assign it there
	If this function is called as an authenticated user, the caller MUST make sure to update the authenticated user
	to the user returned by this function!
	If a non-nil user is returned, it is valid.
	If err == nil, a valid user is returned
*/
func NewProviderUserID(providerName string, tenant string, providerUserID string, registeredUser *types.User) (user *types.User, err error) {
	var pr types.ProviderRecord
	user = new(types.User)

	if tenant == "" {
		tenant = "none"
	}

	err = util.DB.FirstOrCreate(&pr, types.ProviderRecord{
		Implementation: providerName,
		Tenant:         tenant,
		ProviderUserID: providerUserID,
	}).Error
	if err != nil {
		user = nil
		return
	}

preUser:
	if pr.ChiefUserID == 0 {
		// No assigned user. New or unused identity
		if registeredUser.IsValid() {
			user = registeredUser
		} else {
			// No user passed in, create a new one
			err = util.DB.Create(user).Error
			if err != nil {
				user = nil
				return
			}
		}

		// We have a valid user (passed in or created). Assign identity there
		pr.ChiefUserID = user.ID
		err = util.DB.Save(&pr).Error
	} else {
		// Since ChiefUserID is a foreign key, it MUST exist. Or was deleted in a data-race. Then just error.
		err = util.DB.First(user, pr.ChiefUserID).Error
		if err == gorm.ErrRecordNotFound {
			pr.ChiefUserID = 0
			goto preUser
		}
	}

	// Just make sure to return nil if the user is invalid
	if !user.IsValid() {
		user = nil
	}

	return user, err
}

func UnlinkProvider(providerName string, tenant string, user *types.User) (err error) {
	var providerRecord = types.ProviderRecord{}
	if user != nil {
		err = util.DB.First(&providerRecord, types.ProviderRecord{ChiefUserID: user.ID, Implementation: providerName, Tenant: tenant}).Error
	}
	if err != nil {
		return
	}
	if providerRecord.ID != 0 {
		err = util.DB.Unscoped().Delete(&providerRecord).Error
	}

	return
}

func DeleteAccount(user *types.User) (err error) {
	if !user.IsValid() {
		return os.ErrInvalid
	}

	var providerRecords []types.ProviderRecord
	if user != nil {
		err = util.DB.Find(&providerRecords, types.ProviderRecord{ChiefUserID: user.ID}).Error
	}
	if err != nil {
		return
	}
	if user == nil {
		return os.ErrInvalid
	}
	for _, record := range providerRecords {
		if record.ID != 0 {
			err = util.DB.Unscoped().Delete(&record).Error
			if err != nil {
				return
			}
		}
	}

	user.PublicUUID = fmt.Sprintf("SHREDDED_%s", uuid.New().String())
	user.DisplayName = user.PublicUUID
	user.LastRevokedIndex = math.MaxUint32
	user.TokensIssued = math.MaxUint32
	user.CreatedAt = time.Unix(1, 0)
	user.UpdatedAt = time.Unix(1, 0)
	user.DeletedAt = gorm.DeletedAt{
		Time:  time.Unix(1, 0),
		Valid: true,
	}

	// Forced update with soft-delete override the Subject and make sure the UserID is not re-used
	err = util.DB.UpdateColumns(user).Error
	return
}
