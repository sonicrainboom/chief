package providers

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"golang.org/x/oauth2"
	"log"
	"net/http"
)

func LoginByOauth(writer http.ResponseWriter, request *http.Request) {
	var err error
	var provider Provider
	var jar *StateJar
	var vars = mux.Vars(request)
	var providerName = vars["provider"]
	var tenant = vars["tenant"]
	var useUI = request.URL.Query().Get("ui") == "true"

	util.WritePrivacyCookie(writer)

	provider, err = GetProvider(providerName)
	if err != nil {
		err = errors.Wrapf(err, "could not get provider")
		log.Print(err)
		util.EndRequest(writer, request, 500)
		return
	}

	if !provider.ValidateTenant(tenant) {
		err = errors.Wrapf(err, "!provider.ValidateTenant")
		log.Print(err)
		util.EndRequest(writer, request, 400)
		return
	}

	jar, err = MakeStateJar(providerName, tenant, request)
	if err != nil {
		err = errors.Wrapf(err, "could not make state jar")
		log.Print(err)
		util.EndRequest(writer, request, 500)
		return
	}
	jar.ReturnToUI = useUI

	jar.WriteCookie(writer)

	config := provider.Oauth2Config(tenant)
	config.RedirectURL = fmt.Sprintf(
		"%s/api/v1/auth/callback/%s",
		util.BaseURL,
		providerName,
	)

	writer.Header().Set(
		"Location",
		config.AuthCodeURL(string(jar.StateToken), oauth2.AccessTypeOnline),
	)
	writer.WriteHeader(http.StatusFound)
}
