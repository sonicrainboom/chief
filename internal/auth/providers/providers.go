package providers

import (
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"golang.org/x/oauth2"
	"log"
)

type Provider interface {
	Oauth2Config(tenant string) *oauth2.Config
	ValidateTenant(tenant string) bool
	ValidateProviderUserID(userID string, tenant string) bool
	GetUserID(tenant string, token *oauth2.Token) (string, error)
}

var (
	ENoProvider = errors.New("no such provider")
	providers   = map[string]Provider{}
	descriptors = map[string]ProviderDescriptor{}
)

type ProviderDescriptor struct {
	Name               string
	Enabled            bool
	IconClass          string `json:"IconClass,omitempty"`
	ImplementationName string
	Tenants            []string
}

func AddProvider(descriptor ProviderDescriptor, implementation Provider) {
	log.Printf("Adding provider '%s'\n", descriptor.ImplementationName)
	providers[descriptor.ImplementationName] = implementation
	descriptors[descriptor.ImplementationName] = descriptor
}

func GetProvider(implementationName string) (Provider, error) {
	if p, ok := providers[implementationName]; ok {
		return p, nil
	}

	return nil, ENoProvider
}

func GetDescriptors() map[string]ProviderDescriptor {
	return descriptors
}
