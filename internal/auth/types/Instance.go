package types

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

const (
	INSTANCE_BOOTSTRAP_CREATED = iota
	INSTANCE_BOOTSTRAP_KEYS
	INSTANCE_BOOTSTRAP_PORTS
	INSTANCE_BOOTSTRAP_CERTIFICATE
	INSTANCE_BOOTSTRAP_DONE
)

type Instance struct {
	gorm.Model
	OwnerID        uint
	Owner          *User
	PublicUUID     string `gorm:"unique" json:"public_uuid"`            // Auto-generated in-app
	DisplayName    string `gorm:"unique" json:"display_name,omitempty"` // Custom hooks make this empty locally if it equals the Subject
	BootstrapState uint   `gorm:"default:0"`
}

func (instance *Instance) IsValid() bool {
	if instance != nil && instance.ID != 0 && instance.PublicUUID != "" {
		return true
	}
	return false
}

func (instance *Instance) BeforeSave(tx *gorm.DB) (err error) {
	if instance.PublicUUID == "" {
		instance.PublicUUID = uuid.New().String()
	}
	if instance.DisplayName == "" {
		instance.DisplayName = instance.PublicUUID
	}
	return
}

func (instance *Instance) AfterFind(tx *gorm.DB) (err error) {
	if instance.DisplayName == instance.PublicUUID {
		instance.DisplayName = ""
	}
	return
}

func (instance *Instance) AfterSave(tx *gorm.DB) (err error) {
	return instance.AfterFind(tx)
}
