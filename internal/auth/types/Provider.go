package types

import (
	"gorm.io/gorm"
)

type ProviderRecord struct {
	gorm.Model
	ChiefUserID    uint
	Implementation string `gorm:"<-:create;notNull;uniqueIndex:provider_record_index"`
	Tenant         string `gorm:"<-:create;default:none;uniqueIndex:provider_record_index"`
	ProviderUserID string `gorm:"<-:create;notNull;uniqueIndex:provider_record_index"`
}
