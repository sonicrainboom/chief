package types

import (
	"github.com/google/uuid"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gorm.io/gorm"
)

var ErrNoUser = errors.New("no valid user provided")

type User struct {
	gorm.Model       `json:"-"`
	ProviderRecords  []ProviderRecord `gorm:"foreignKey:ChiefUserID" json:"-"`
	PublicUUID       string           `gorm:"unique" json:"public_uuid"`            // Auto-generated in-app
	DisplayName      string           `gorm:"unique" json:"display_name,omitempty"` // Custom hooks make this empty locally if it equals the Subject
	TokensIssued     uint             `json:"-"`
	LastRevokedIndex uint             `json:"-"`
}

func (u *User) IsValid() bool {
	if u != nil && u.ID != 0 && u.PublicUUID != "" {
		return true
	}
	return false
}

func (u *User) BeforeSave(tx *gorm.DB) (err error) {
	if u.PublicUUID == "" {
		u.PublicUUID = uuid.New().String()
	}
	if u.DisplayName == "" {
		u.DisplayName = u.PublicUUID
	}
	return
}

func (u *User) AfterFind(tx *gorm.DB) (err error) {
	if u.DisplayName == u.PublicUUID {
		u.DisplayName = ""
	}
	return
}

func (u *User) AfterSave(tx *gorm.DB) (err error) {
	return u.AfterFind(tx)
}
