package errors

import "github.com/pkg/errors"

func Wrap(err error, fmtAndArgs ...interface{}) error {
	if len(fmtAndArgs) >= 1 {
		fmt, ok := fmtAndArgs[0].(string)
		if !ok {
			fmt = "%+v"
			return errors.Wrapf(err, fmt, fmtAndArgs...)
		}
		if len(fmtAndArgs) >= 2 {
			return errors.Wrapf(err, fmt, fmtAndArgs[1:]...)
		} else {
			return errors.Wrap(err, fmt)
		}
	}
	return errors.Wrap(err, "")
}

func Wrapf(err error, fmtAndArgs ...interface{}) error {
	return Wrap(err, fmtAndArgs...)
}

func New(message string) error {
	return errors.New(message)
}

func Is(err error, target error) bool {
	return errors.Is(err, target)
}
