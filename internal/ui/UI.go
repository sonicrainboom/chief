package ui

import (
	"github.com/gomarkdown/markdown"
	"gitlab.com/sonicrainboom/chief/generated_assets/frontend_dist"
	"gitlab.com/sonicrainboom/chief/internal/auth/AuthJar"
	"gitlab.com/sonicrainboom/chief/internal/auth/providers"
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"html/template"
	"log"
	"net/http"
	"strings"
)

var UI *UIType

func init() {
	UI = new(UIType)
	var funcs = template.FuncMap{"join": strings.Join}
	UI.tpl = template.New("root").Funcs(funcs)
	for _, file := range frontend_dist.AssetNames() {
		if strings.HasSuffix(file, ".html") {
			UI.addTemplate(file)
		}
		if strings.HasSuffix(file, ".md") {
			UI.addTemplateMarkdown(file)
		}
	}
}

func (ui *UIType) addTemplate(file string) {
	f, err := frontend_dist.Asset(file)
	if err != nil {
		panic(err)
	}
	template.Must(UI.tpl.New(strings.ReplaceAll(file, ".html", "")).Parse(string(f)))
}

func (ui *UIType) addTemplateMarkdown(file string) {
	f, err := frontend_dist.Asset(file)
	if err != nil {
		panic(err)
	}
	template.Must(UI.tpl.New(strings.ReplaceAll(file, ".md", "")).Parse(string(markdown.ToHTML(f, nil, nil))))
}

type UIType struct {
	tpl *template.Template
}

type UIProviderDescriptor struct {
	providers.ProviderDescriptor
	LinkedByUser bool
}

type UIContext struct {
	Title          string
	BaseURL        string
	User           *types.User
	Providers      map[string]*UIProviderDescriptor
	CodeBox        string
	Message        string
	MessageType    string
	PrivacyPending bool
}

func (ui *UIType) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	if strings.HasPrefix(request.URL.Path, "/api") {
		writer.WriteHeader(404)
		return
	}

	var context = UIContext{
		Title:          "SonicRainBoom Account Center",
		BaseURL:        util.BaseURL,
		PrivacyPending: !util.IsPrivacyCookieSet(request),
	}
	var tpl = "ACP"

	if strings.HasPrefix(request.URL.Path, "/privacy") {
		tpl = "Privacy"
		context.PrivacyPending = false
		if util.DidRevalidateExplicitCache(writer, request, "") {
			return
		}
	} else {
		renderACPContext(&context, request)
	}

	err := ui.tpl.Lookup(tpl).Execute(writer, context)
	if err != nil {
		log.Printf("Template error: %+v", err)
	}
}

func renderACPContext(context *UIContext, request *http.Request) {
	var provs = providers.GetDescriptors()
	context.Providers = map[string]*UIProviderDescriptor{}
	for k, prov := range provs {
		context.Providers[k] = &UIProviderDescriptor{ProviderDescriptor: prov}
	}

	var auth *AuthJar.AuthJar
	//var err error

	auth, _ = AuthJar.AuthJarByRequest(request)
	context.User, _ = auth.GetUser()

	var providerRecords []types.ProviderRecord
	if context.User != nil {
		util.DB.Find(&providerRecords, types.ProviderRecord{ChiefUserID: context.User.ID})

		for _, pr := range providerRecords {
			if _, ok := context.Providers[pr.Implementation]; ok {
				context.Providers[pr.Implementation].LinkedByUser = true
			}
		}
	}

	switch request.URL.Query().Get("status") {
	default:
	case "200":
		context.Message = "Success!"
		context.MessageType = "success"
	case "400":
		context.Message = "Bad request"
		context.MessageType = "danger"
	case "404":
		context.Message = "Not found"
		context.MessageType = "danger"
	case "403":
		context.Message = "Forbidden"
		context.MessageType = "danger"
	case "500":
		context.Message = "Internal Server Error"
		context.MessageType = "danger"
	}
	instance := request.URL.Query().Get("instance")
	state := request.URL.Query().Get("state")
	token := request.URL.Query().Get("token")
	if instance != "" && state == "0" && token != "" {
		context.Message = "New server registered! Instance ID: " + instance + ". Registration token:"
		context.MessageType = "success"
		context.CodeBox = token
	}

	switch request.URL.Query().Get("postLoginStatus") {
	case "":
	case "200":
		context.Message = "Login successful!"
		context.MessageType = "success"
	default:
		context.Message = "Error during login!"
		context.MessageType = "danger"
	}
}
