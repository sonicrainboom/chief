package api

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/sonicrainboom/chief/internal/auth/AuthJar"
	"gitlab.com/sonicrainboom/chief/internal/auth/providers"
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/config"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/instances"
	"gitlab.com/sonicrainboom/chief/internal/ui"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"gitlab.com/sonicrainboom/chief/internal/util/connection"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	_ "gitlab.com/sonicrainboom/chief/internal/auth/providers/AppleID"
	_ "gitlab.com/sonicrainboom/chief/internal/auth/providers/Facebook"
	_ "gitlab.com/sonicrainboom/chief/internal/auth/providers/GitLab"
)

type API struct {
	*mux.Router
}

// https://stackoverflow.com/a/55329317/3433727
func saveConnInContext(ctx context.Context, c net.Conn) context.Context {
	return context.WithValue(ctx, connection.ConnContextKey, c)
}

func StartAPI() {
	providers.Init()
	AuthJar.Init()
	instances.Init()
	api := API{mux.NewRouter()}

	api.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
			if strings.HasPrefix(r.URL.Path, "/api") {
				w.Header().Set("content-type", "application/json; charset=utf-8")
				w.Header().Set("Access-Control-Allow-Credentials", "true")
				w.Header().Set("Access-Control-Allow-Origin", "*")
				w.Header().Set("Vary", "Origin")
			}
			if strings.HasSuffix(r.URL.Path, ".js") {
				w.Header().Set("content-type", "application/javascript; charset=utf-8")
			}

			jar, err := AuthJar.AuthJarByRequest(r)
			if jar != nil && err != http.ErrNoCookie {
				user, _ := jar.GetUser()
				if user == nil {
					AuthJar.ResetAuthCookie(w)
				} else {
					if jar.Expires < time.Now().Add(6*time.Hour).Unix() {
						jar.WriteCookie(w)
					}
				}
			}

			defer func() {
				if err := recover(); err != nil && err != http.ErrAbortHandler {
					w.Header().Del("content-type")
					w.Header().Del("content-length")
					w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
					w.WriteHeader(500)
					panic(http.ErrAbortHandler) // This signalizes, that an error was already handled.
				}
			}()
			next.ServeHTTP(w, r)
		})
	})

	api.Path("/api/v1/instance/register").Methods("GET").HandlerFunc(api.registerInstance)

	api.Path("/api/v1/users/by-uuid/{uuid}").Methods("GET").HandlerFunc(api.getUserBy)
	api.Path("/api/v1/users/by-name/{name}").Methods("GET").HandlerFunc(api.getUserBy)

	api.Path("/api/v1/keys/auth").Methods("GET").HandlerFunc(api.getAuthKeys)
	api.Path("/api/v1/keys/instance").Methods("GET").HandlerFunc(api.getInstanceKeys)

	api.Path("/api/v1/auth/login/{provider:[a-z0-9._-]+}/{tenant}").Methods("GET").HandlerFunc(providers.LoginByOauth)
	api.Path("/api/v1/auth/login/{provider:[a-z0-9._-]+}").Methods("GET").HandlerFunc(providers.LoginByOauth)
	api.Path("/api/v1/auth/unlink/{provider:[a-z0-9._-]+}/{tenant").Methods("GET").HandlerFunc(api.unlinkProvider)
	api.Path("/api/v1/auth/unlink/{provider:[a-z0-9._-]+}").Methods("GET").HandlerFunc(api.unlinkProvider)
	api.Path("/api/v1/auth/callback/{provider:[a-z0-9._-]+}").Methods("GET").HandlerFunc(providers.Callback)
	api.Path("/api/v1/auth/revoke").Methods("GET").HandlerFunc(api.revoke)
	api.Path("/api/v1/auth/deleteAccount").Methods("GET").HandlerFunc(api.deleteAccount)

	api.Path("/api/v1/acceptPrivacy").Methods("GET").HandlerFunc(api.acceptPrivacy)

	api.Path("/api/v1/auth/debug/getState").Methods("GET").HandlerFunc(api.getState)
	api.Path("/api/v1/auth/debug/checkState").Methods("GET").HandlerFunc(api.checkState)

	// Prometheus
	api.PathPrefix("/metrics").Handler(promhttp.Handler())

	api.NotFoundHandler = ui.UI

	http.Handle("/", api)
	server := http.Server{
		Addr:        config.Config.ListenAddr,
		ConnContext: saveConnInContext,
	}

	var err error
	if config.Config.TLSCert != "" {
		err = server.ListenAndServeTLS(config.Config.TLSCert, config.Config.TLSKey)
	} else {
		err = server.ListenAndServe()
	}

	if err != nil {
		panic(err)
	}
}

func (api *API) getUserBy(writer http.ResponseWriter, request *http.Request) {
	var vars = mux.Vars(request)
	var uuid = vars["uuid"]
	var name = vars["name"]
	var user types.User
	var err error

	if uuid == "" && name == "" || uuid != "" && name != "" {
		err = errors.New("must provide ONE of uuid or name")
	} else {
		if uuid != "" {
			err = util.DB.Where(types.User{PublicUUID: uuid}).First(&user).Error
			user.DisplayName = "" // If queried via the Subject, the querier does not know the name. Don't provide it.
		} else if name != "" {
			err = util.DB.Where(types.User{DisplayName: name}).First(&user).Error
		}
	}

	if err != nil {
		log.Printf("getUserBy(%s, %s): %+v", uuid, name, err)
	}

	if user.ID == 0 {
		util.EndRequest(writer, request, 404)
		return
	}

	util.RenderJSON(writer, user, 200)
}

func (api *API) getState(writer http.ResponseWriter, request *http.Request) {
	jar, err := providers.MakeStateJar("debug", "", request)
	if err != nil {
		log.Printf("getState(): MakeStateJar %+v", err)
	}

	jar.WriteCookie(writer)

	util.RenderJSON(writer, jar, 200)
}

func (api *API) checkState(writer http.ResponseWriter, request *http.Request) {
	var ok bool

	var jar, err = providers.StateJarByRequest(request)
	if err != nil {
		log.Printf("%+v", errors.Wrap(err))
	}
	providers.ResetStateCookie(writer)
	ok = jar.Validate("debug", "", request)
	if !ok {
		util.EndRequest(writer, request, 403)
		return
	}

	util.RenderJSON(writer, jar, 200)
}

func (api *API) getAuthKeys(writer http.ResponseWriter, r *http.Request) {
	getKeys(AuthJar.KeyJar, writer, r)
}

func getKeys(jar *util.KeyJar, writer http.ResponseWriter, r *http.Request) {
	var ETag = fmt.Sprintf("\"%s-%d\"", jar.SigningKeyID, len(jar.JWKS.Keys))
	if !util.DidRevalidateExplicitCache(writer, r, ETag) {
		util.RenderJSON(writer, jar.JWKS, 200)
	}
}

func (api *API) revoke(writer http.ResponseWriter, request *http.Request) {
	var jar *AuthJar.AuthJar
	var user *types.User
	var err error

	util.WritePrivacyCookie(writer)

	jar, _ = AuthJar.AuthJarByRequest(request)
	user, err = jar.GetUser()
	if err == nil && user != nil {
		err = errors.Wrapf(err, "GetUser failed")
		user.LastRevokedIndex = user.TokensIssued
		util.DB.Save(user)
		log.Printf("Revoked all tokens for user %s. Current Index: %d", user.PublicUUID, user.LastRevokedIndex)
	}
	AuthJar.ResetAuthCookie(writer)
	util.EndRequest(writer, request, 200)
	return
}

func (api *API) unlinkProvider(writer http.ResponseWriter, request *http.Request) {
	var vars = mux.Vars(request)
	var provider = vars["provider"]
	var tenant = vars["tenant"]
	var jar *AuthJar.AuthJar
	var user *types.User
	var err error

	util.WritePrivacyCookie(writer)

	jar, err = AuthJar.AuthJarByRequest(request)
	if err != nil {
		log.Printf("unlinkProvider(): AuthJarByRequest %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	user, err = jar.GetUser()
	if err != nil {
		log.Printf("unlinkProvider(): GetUser %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	err = providers.UnlinkProvider(provider, tenant, user)
	if err != nil {
		log.Printf("unlinkProvider(): UnlinkProvider %+v", err)
		util.EndRequest(writer, request, 500)
		return
	}

	util.EndRequest(writer, request, 200)
	return
}

func (api *API) deleteAccount(writer http.ResponseWriter, request *http.Request) {
	var jar *AuthJar.AuthJar
	var user *types.User
	var err error

	util.WritePrivacyCookie(writer)

	jar, err = AuthJar.AuthJarByRequest(request)
	if err != nil {
		log.Printf("deleteAccount(): AuthJarByRequest %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	user, err = jar.GetUser()
	if err != nil {
		log.Printf("deleteAccount(): GetUser %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	err = providers.DeleteAccount(user)
	if err != nil {
		log.Printf("deleteAccount(): DeleteAccount %+v", err)
		util.EndRequest(writer, request, 500)
		return
	}

	util.EndRequest(writer, request, 200)
}

func (api *API) registerInstance(writer http.ResponseWriter, request *http.Request) {
	var jar *AuthJar.AuthJar
	var user *types.User
	var err error

	util.WritePrivacyCookie(writer)

	jar, err = AuthJar.AuthJarByRequest(request)
	if err != nil {
		log.Printf("registerInstance(): AuthJarByRequest %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	user, err = jar.GetUser()
	if err != nil || user == nil {
		log.Printf("registerInstance(): GetUser %+v", err)
		util.EndRequest(writer, request, 403)
		return
	}

	var instance = new(types.Instance)
	instance.OwnerID = user.ID
	instance.BootstrapState = types.INSTANCE_BOOTSTRAP_CREATED
	err = util.DB.Create(instance).Error
	if err != nil {
		log.Printf("registerInstance(): Create %+v", err)
		util.EndRequest(writer, request, 500)
		return
	}

	var token instances.InstanceRegistrationToken
	token, err = instances.GenerateInstanceRegistrationToken(instance)
	if err != nil {
		log.Printf("registerInstance(): GenerateInstanceRegistrationToken %+v", err)
		util.EndRequest(writer, request, 500)
		return
	}

	util.EndRequest(writer, request, 200, fmt.Sprintf("instance=%s&state=%d&token=%s", instance.PublicUUID, instance.BootstrapState, token))
	return
}

func (api *API) getInstanceKeys(writer http.ResponseWriter, request *http.Request) {
	getKeys(instances.KeyJar, writer, request)
}

func (api *API) acceptPrivacy(writer http.ResponseWriter, request *http.Request) {
	util.WritePrivacyCookie(writer)
	writer.Header().Set("Location", "/")
	writer.WriteHeader(302)
}
