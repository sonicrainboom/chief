package database

import (
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/util"
)

func init() {
	util.DBOpen()

	var err error

	err = util.DB.AutoMigrate(&types.User{}, &types.ProviderRecord{}, &util.KeyJar{}, &util.KeyJarPrivateKey{}, &types.Instance{})
	if err != nil {
		panic(err)
	}
}
