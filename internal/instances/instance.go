package instances

import (
	"gitlab.com/sonicrainboom/chief/internal/auth/types"
	"gitlab.com/sonicrainboom/chief/internal/errors"
	"gitlab.com/sonicrainboom/chief/internal/util"
	"gitlab.com/sonicrainboom/chief/shared"
	"regexp"
	"time"
)

type InstanceRegistrationToken string

var r = regexp.MustCompile(`^[a-z0-9]{64}$`)

func (t InstanceRegistrationToken) ValidFormat() bool {
	return r.MatchString(string(t))
}

func GenerateInstanceRegistrationToken(instance *types.Instance) (InstanceRegistrationToken, error) {
	var irv = shared.InstanceRegistrationVoucher{
		Subject:   instance.PublicUUID,
		Issuer:    util.BaseURL,
		TokenType: "registration",
		IssuedAt:  time.Now().Unix(),
	}

	token, err := KeyJar.CreateJWT(irv)
	return InstanceRegistrationToken(token), errors.Wrap(err)
}
