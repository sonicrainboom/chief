package instances

import (
	"gitlab.com/sonicrainboom/chief/internal/util"
	"sync"
)

var KeyJar *util.KeyJar
var mtx sync.Mutex

func Init() {
	mtx.Lock()
	defer mtx.Unlock()
	if KeyJar == nil {
		KeyJar = util.GetKeyJar("instance_voucher", true)
	}
}
