package config

import (
	"golang.org/x/sys/unix"
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

var Config *ConfigType

type ConfigType struct {
	AppleID struct {
		DeveloperTeamID string `yaml:"developer_team_id"` // https://developer.apple.com/account/resources / 10-digits in the top right
		KeyIdentifier   string `yaml:"key_identifier"`    // https://developer.apple.com/account/resources/authkeys/list
		ServiceID       string `yaml:"service_id"`        // https://developer.apple.com/account/resources/identifiers/list/serviceId
		PrivateKeyPath  string `yaml:"private_key_path"`  // Only required during bootstrap
	} `yaml:"apple_id"`
	Facebook struct {
		ClientID     string `yaml:"client_id"`
		ClientSecret string `yaml:"client_secret"`
	} `yaml:"facebook"`
	GitLab struct {
		ClientID     string `yaml:"client_id"`
		ClientSecret string `yaml:"client_secret"`
	} `yaml:"gitlab"`
	KeyJarPassphrase string   `yaml:"key_jar_passphrase"`
	DatabasePath     string   `yaml:"database_path"`
	TrustedProxies   []string `yaml:"trusted_proxies"`
	ListenAddr       string   `yaml:"listen_addr"`
	TLSCert          string   `yaml:"tls_cert"`
	TLSKey           string   `yaml:"tls_key"`
}

func init() {
	if os.Getenv("NO_MLOCK") != "true" {
		err := unix.Mlockall(unix.MCL_FUTURE | unix.MCL_CURRENT)
		if err != nil {
			log.Fatal("Could not lock memory. Is the capability missing? You can also set NO_MLOCK=true")
		}
	}

	file, err := os.Open("config.yml")
	if err != nil {
		panic(err)
	}
	var config ConfigType
	err = yaml.NewDecoder(file).Decode(&config)
	if err != nil {
		panic(err)
	}
	Config = &config
}
