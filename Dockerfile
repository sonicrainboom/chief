FROM golang:1.15-alpine as builder
COPY . /build
WORKDIR /build
ENV GOPROXY=direct
RUN apk add --no-cache build-base ca-certificates git
RUN go generate -v ./cmd/chief.go
RUN go build -buildmode=pie -v -a -trimpath -ldflags="-w -s -extldflags=-Wl,-z,now,-z,relro" ./cmd/chief.go

FROM alpine:latest
CMD /usr/bin/chief
RUN apk add --no-cache ca-certificates
COPY --from=builder /build/chief /usr/bin/chief
