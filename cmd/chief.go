package main

//go:generate go run ../generators/frontend.go -dir=frontend/dist

import (
	"gitlab.com/sonicrainboom/chief/internal/api"

	// Imports for side-effects
	_ "gitlab.com/sonicrainboom/chief/internal/database"
)

func main() {
	api.StartAPI()
}
