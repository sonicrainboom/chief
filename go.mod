module gitlab.com/sonicrainboom/chief

go 1.15

require (
	github.com/go-bindata/go-bindata/v3 v3.1.3 // indirect
	github.com/gomarkdown/markdown v0.0.0-20201109183857-6ac05fb9e0a4
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.14.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	golang.org/x/sys v0.0.0-20200803210538-64077c9b5642
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.5
)
