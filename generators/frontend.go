// +build ignore

package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"flag"
	"fmt"
	"github.com/go-bindata/go-bindata/v3"
	"github.com/prometheus/common/log"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

func main() {
	dir := flag.String("dir", "", "directory to pack")
	flag.Parse()
	if *dir == "" {
		panic("no -dir provided")
	}
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	if strings.HasSuffix(wd, "/cmd") {
		os.Chdir("../")
	}
	wd, err = os.Getwd()
	if err != nil {
		panic(err)
	}

	var assetDir = *dir

	c := bindata.NewConfig()
	c.NoMemCopy = true
	c.Output = path.Join(wd, "generated_assets", sanitizeName(assetDir), "bindata.go")
	c.HttpFileSystem = true
	if os.Getenv("DEBUG") != "1" {
		// Generate random asset IDs for each build or use $pkgver
		var assetId string
		if os.Getenv("pkgver") != "" {
			assetId = os.Getenv("pkgver")
		} else {
			h := sha256.New()
			buf := make([]byte, 512)
			n, _ := rand.Read(buf)
			if n != len(buf) {
				panic(io.ErrShortBuffer)
			}
			h.Write(buf)
			assetId = fmt.Sprintf("build-%x", h.Sum(nil))[0:14]
		}
		log.Infoln(assetId)

		dir, err := ioutil.TempDir("", fmt.Sprintf("assets_%s_%s", sanitizeName(assetDir), assetId))
		if err != nil {
			panic(err)
		}
		defer os.RemoveAll(dir)

		matches := cpDir(assetDir, assetId, dir)

		err = os.Chdir(dir)
		if err != nil {
			panic(err)
		}

		log.Info(dir)

		matches, _ = filepath.Glob( "./*")
		log.Infof("%+v\n", matches)
	}

	c.Prefix = assetDir
	c.Debug = os.Getenv("DEBUG") == "1"
	c.Input = []bindata.InputConfig{
		{
			Path:      filepath.Clean(assetDir),
			Recursive: true,
		},
	}
	c.Package = sanitizeName(assetDir)

	err = bindata.Translate(c)
	if err != nil {
		panic(err)
	}
}

var sanitizer = regexp.MustCompile(`(?m)[^a-zA-Z0-9]+`)

func sanitizeName(in string) string {
	return sanitizer.ReplaceAllString(in, "_")
}



func cpDir(assetDir string, assetId string, dir string) []string {
	log.Infof("dir: %s", assetDir)
	matches, _ := filepath.Glob(assetDir + "/*")
	log.Infof("matches: %s", matches)

	assets := map[string]string{}

	for _, file := range matches {
		if !strings.HasSuffix(file, ".html") && !strings.HasSuffix(file, ".md") && !strings.Contains(file, "/fa-") {
			if info , err := os.Stat(file); err != nil && info.IsDir() {
				continue
			}
			// Alter filename
			ext := filepath.Ext(file)
			newFile := strings.ReplaceAll(file, ext, "_"+assetId+ext)
			newPath := path.Join(dir, newFile)

			err := os.MkdirAll(filepath.Dir(newPath), 0777)
			if err != nil {
				panic(err)
			}

			// Copy file
			data, err := ioutil.ReadFile(file)
			if err != nil {
				panic(err)
			}
			data = bytes.ReplaceAll(data, []byte("$pkgver"), []byte(assetId))
			err = ioutil.WriteFile(path.Join(dir, newFile), data, 0666)
			if err != nil {
				panic(err)
			}

			// Add to potentials asset list
			assets[path.Base(file)] = path.Base(newFile)
		}
	}

	for _, file := range matches {
		if strings.HasSuffix(file, ".html") || strings.HasSuffix(file, ".md") || strings.Contains(file, "/fa-") {
			if info , err := os.Stat(file); err != nil && info.IsDir() {
				continue
			}
			// Alter filename
			newPath := path.Join(dir, file)

			err := os.MkdirAll(filepath.Dir(newPath), 0777)
			if err != nil {
				panic(err)
			}

			// Replace assets
			data, err := ioutil.ReadFile(file)
			if err != nil {
				panic(err)
			}

			for source, target := range assets {
				data = bytes.ReplaceAll(data, []byte(source), []byte(target))
			}
			data = bytes.ReplaceAll(data, []byte("$pkgver"), []byte(assetId))

			err = ioutil.WriteFile(path.Join(dir, file), data, 0666)
			if err != nil {
				panic(err)
			}
		}
	}

	fmt.Printf("%+v\n", assets)
	fmt.Printf("%+v\n", matches)
	return matches
}
